(function () {
    var i = 0;
    checkStatus(init);
    jQuery(".processing").css('display', 'block');

    function checkStatus(callback) {
        jQuery.get(faker_status+upload_id+(i===0?'/?start':''))
            .done(function (response) {
                var res = JSON.parse(response);
                if(res.message=="1") {
                    callback.call(this, true);
                }
                else {
                    jQuery(".processing p").html(res.message);
                    callback.call(this);
                }
            });
        i=1;
    }
    function init(done) {
        if(done) {
            completed();
            return;
        }
        setTimeout(function () {
            checkStatus(init);
        }, 5000);
    }
    function completed() {
        jQuery(".processing").css('display', 'none');
        jQuery(".processed").css('display', 'block');
        download_zip.call(this);
    }
    function download_zip() {
        jQuery(".processed p").html('Downloading faker images');
        jQuery.post(ajaxurl, {action:'download_zip', id: upload_id, import_id: import_id}, function (response) {
            if(JSON.parse(response).ok==1) {
                jQuery(".processed p").html('Done. Proceeding to import');
                jQuery("#submit-on-completion").submit();
            }
        });
    }
})();
